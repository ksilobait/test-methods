package ru.nsu.fit.endpoint.manager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.nsu.fit.endpoint.database.IDBService;

import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.data.PlanPojo;


class PlanManagerTest {
  private IDBService dbService;
  private PlanManager planManager;
  private PlanPojo initPlan;

  @BeforeEach
  void init() {
    // create stubs for the test's class
    this.dbService = mock(IDBService.class);
    Logger logger = mock(Logger.class);

    // create the test's class
    this.planManager = new PlanManager(this.dbService, logger);

    this.initPlan = new PlanPojo();
    this.initPlan.name = "adventures of the Meowox cat";
    this.initPlan.details = "follow the unbarkable Meowox in the Puggerland";
    this.initPlan.fee = 300;
  }

  @Test
  void createPlan() {
    PlanPojo finalPlan = new PlanPojo();
    finalPlan.id = UUID.randomUUID();
    finalPlan.name = this.initPlan.name;
    finalPlan.details = this.initPlan.details;
    finalPlan.fee = this.initPlan.fee;

    Mockito.when(this.dbService.createPlan(this.initPlan)).thenReturn(finalPlan);
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException("Plan with name '" + this.initPlan.name + " was not found"));

    // Вызываем метод, который хотим протестировать
    PlanPojo thePlan = this.planManager.createPlan(this.initPlan);

    // Проверяем результат выполенния метода
    assertEquals(thePlan.id, finalPlan.id);

    // Проверяем, что метод по созданию Plan был вызван ровно 1 раз с определенными аргументами
    Mockito.verify(this.dbService, Mockito.times(1)).createPlan(this.initPlan);
    Mockito.verify(this.dbService, Mockito.times(1)).getPlanIDByName(this.initPlan.name);

    // Проверяем, что другие методы не вызывались
    Mockito.verify(this.dbService, Mockito.times(0)).getPlans();
  }

  @Test
  void createPlanWithCorrectName() {
    this.initPlan.name = "EA";
    assertEquals(this.initPlan.name.length(), 2);
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException("Plan with name '" + this.initPlan.name + " was not found"));
    this.planManager.createPlan(this.initPlan);

    this.initPlan.name = String.join("", Collections.nCopies(128, "q"));
    assertEquals(this.initPlan.name.length(), 128);
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException("Plan with name '" + this.initPlan.name + " was not found"));
    this.planManager.createPlan(this.initPlan);
  }

  @Test
  void createPlanWithNullArgument() {
    Exception exception = assertThrows(IllegalArgumentException.class, () ->
        this.planManager.createPlan(null));
    assertEquals("'plan' is null", exception.getMessage());
  }

  @Test
  void createPlanWithSmallName() {
    this.initPlan.name = "";

    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException(
            "Plan with name '" + this.initPlan.name + " was not found"));

    assertEquals(this.initPlan.name.length(), 0);
    try {
      this.planManager.createPlan(this.initPlan);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(),
          "the length of the name should be between 1 and 128 symbols");
    }
  }

  @Test
  void createPlanWithBigName() {
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException(
            "Plan with name '" + this.initPlan.name + " was not found"));

    this.initPlan.name = String.join("", Collections.nCopies(129, "q"));
    assertEquals(this.initPlan.name.length(), 129);
    try {
      this.planManager.createPlan(this.initPlan);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(), "the length of the name should be between 1 and 128 symbols");
    }
  }

  @Test
  void createPlanWithIllegalName() {
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException(
            "Plan with name '" + this.initPlan.name + " was not found"));

    this.initPlan.name = "☠☢☣☎♚";
    assertEquals(this.initPlan.name.length(), 5);
    try {
      this.planManager.createPlan(this.initPlan);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(),
          "name has special symbols");
    }
  }

  @Test
  void createPlanWithDuplicateName() {
    when(this.dbService.getPlanIDByName(this.initPlan.name))
        .then(invocation -> {
          String name = invocation.getArgument(0);
          if (this.initPlan.name.equalsIgnoreCase(name)) {
            return null;
          }
          throw new IllegalArgumentException("Plan with name " + this.initPlan.name + " was not found");
        });
    try {
      this.planManager.createPlan(this.initPlan);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(), "the name should be unique");
    }
  }

  @Test
  void createPlanWithCorrectDetails() {
    this.initPlan.details = "f";
    assertEquals(this.initPlan.details.length(), 1);
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException("Plan with name '" + this.initPlan.name + " was not found"));
    this.planManager.createPlan(this.initPlan);

    this.initPlan.details = String.join("", Collections.nCopies(1024, "0"));
    assertEquals(this.initPlan.details.length(), 1024);
    this.planManager.createPlan(this.initPlan);
  }

  @Test
  void createPlanWithSmallDetails() {
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException(
            "Plan with name '" + this.initPlan.name + " was not found"));

    this.initPlan.details = "";
    assertEquals(this.initPlan.details.length(), 0);
    try {
      this.planManager.createPlan(this.initPlan);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(),
          "the length of the details should be between 1 and 1024 symbols");
    }
  }

  @Test
  void createPlanWithBigDetails() {
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException(
            "Plan with name '" + this.initPlan.name + " was not found"));

    this.initPlan.details = String.join("", Collections.nCopies(1025, "0"));
    assertEquals(this.initPlan.details.length(), 1025);
    try {
      this.planManager.createPlan(this.initPlan);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(),
          "the length of the details should be between 1 and 1024 symbols");
    }
  }

  @Test
  void createPlanWithCorrectFee() {
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException("Plan with name '" + this.initPlan.name + " was not found"));

    this.initPlan.fee = 0;
    this.planManager.createPlan(this.initPlan);

    this.initPlan.fee = 999999;
    this.planManager.createPlan(this.initPlan);
  }

  @Test
  void createPlanWithSmallFee() {
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException(
            "Plan with name '" + this.initPlan.name + " was not found"));

    this.initPlan.fee = -1;
    try {
      this.planManager.createPlan(this.initPlan);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(),
          "the fee should be between 0 and 999999");
    }
  }

  @Test
  void createPlanWithBigFee() {
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException(
            "Plan with name '" + this.initPlan.name + " was not found"));

    this.initPlan.fee = 999999 + 1;
    try {
      this.planManager.createPlan(this.initPlan);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(),
          "the fee should be between 0 and 999999");
    }
  }

  @Test
  void updatePlan() {
    PlanPojo updatePlanOutput = new PlanPojo();
    updatePlanOutput.id = this.initPlan.id;
    updatePlanOutput.name = this.initPlan.name;
    updatePlanOutput.details = this.initPlan.details;
    updatePlanOutput.fee = this.initPlan.fee;

    PlanPojo getPlanOutput = new PlanPojo();
    getPlanOutput.id = this.initPlan.id;
    getPlanOutput.name = this.initPlan.name;
    getPlanOutput.details = this.initPlan.details;
    getPlanOutput.fee = this.initPlan.fee;

    Mockito.when(this.dbService.updatePlan(this.initPlan)).thenReturn(updatePlanOutput);
    Mockito.when(this.dbService.getPlanByID(this.initPlan.id)).thenReturn(getPlanOutput);
    Mockito.when(this.dbService.getPlanIDByName(this.initPlan.name))
        .thenThrow(new IllegalArgumentException("Customer with login '" + this.initPlan.name + " was not found"));

    // Вызываем метод, который хотим протестировать
    PlanPojo thePlan = this.planManager.updatePlan(this.initPlan);

    // Проверяем результат выполенния метода
    assertEquals(thePlan, updatePlanOutput);

    // Проверяем, что метод по созданию Plan был вызван ровно 1 раз с определенными аргументами
    Mockito.verify(this.dbService, Mockito.times(1)).updatePlan(this.initPlan);

    // Проверяем, что другие методы не вызывались
    Mockito.verify(this.dbService, Mockito.times(0)).getPlans();
  }

  @Test
  void updatePlanWithNullArgument() {
    Exception exception = assertThrows(IllegalArgumentException.class, () ->
        this.planManager.updatePlan(null));
    assertEquals("'plan' is null", exception.getMessage());
  }

  @Test
  void removePlan() {
    UUID id = UUID.randomUUID();
    this.planManager.removePlan(id);
    verify(this.dbService, times(1)).removePlanByID(id);
    verify(this.dbService, times(1)).removePlanByID(any());
    verify(this.dbService, times(0)).getPlans();
  }
}
