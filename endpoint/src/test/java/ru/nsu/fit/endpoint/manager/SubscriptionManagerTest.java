package ru.nsu.fit.endpoint.manager;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;

class SubscriptionManagerTest {
  private IDBService dbService;
  private SubscriptionManager subscriptionManager;

  private CustomerPojo initCustomer;
  private PlanPojo initPlan;
  private SubscriptionPojo initSub;

  @BeforeEach
  void init() {
    // create stubs for the test's class
    this.dbService = mock(IDBService.class);
    Logger logger = mock(Logger.class);

    // create the test's class
    this.subscriptionManager = new SubscriptionManager(this.dbService, logger);

    this.initCustomer = new CustomerPojo();
    this.initCustomer.id = UUID.randomUUID();
    this.initCustomer.firstName = "John";
    this.initCustomer.lastName = "Wick";
    this.initCustomer.login = "john@mail.ru";
    this.initCustomer.pass = "123!@#qweQWE";
    this.initCustomer.balance = 666;
    this.dbService.createCustomer(this.initCustomer);

    this.initPlan = new PlanPojo();
    this.initPlan.id = UUID.randomUUID();
    this.initPlan.name = "adventures of the Meowox cat";
    this.initPlan.details = "follow the unbarkable Meowox in the Puggerland";
    this.initPlan.fee = 300;
    this.dbService.createPlan(this.initPlan);

    this.initSub = new SubscriptionPojo();
    this.initSub.id = UUID.randomUUID();
    this.initSub.planId = this.initPlan.id;
    this.initSub.customerId = this.initCustomer.id;
  }

  @Test
  void createSubscription() {
    SubscriptionPojo finalSub = new SubscriptionPojo();
    finalSub.id = UUID.randomUUID();
    finalSub.customerId = this.initSub.customerId;
    finalSub.planId = this.initSub.planId;

    Mockito.when(this.dbService.getSubscriptionByID(this.initSub.id))
        .thenThrow(new IllegalArgumentException("there's already such subscription"));
//        .thenThrow(new Exception("hi"));
    Mockito.when(this.dbService.getCustomerByID(this.initSub.customerId)).thenReturn(this.initCustomer);
    Mockito.when(this.dbService.getPlanByID(this.initSub.planId)).thenReturn(this.initPlan);
    Mockito.when(this.dbService.createSubscription(eq(this.initSub), anyInt()))
            .thenReturn(finalSub);

    // Вызываем метод, который хотим протестировать
    SubscriptionPojo theSub = this.subscriptionManager.createSubscription(this.initSub);

    // Проверяем результат выполенния метода
    assertEquals(theSub.id, finalSub.id);

    // Проверяем, что другие методы не вызывались
    Mockito.verify(this.dbService, Mockito.times(0)).getSubscriptions(any());
  }

  @Test
  void createSubscriptionCorrectly() {
    Mockito.when(this.dbService.getSubscriptionByID(this.initSub.id))
            .thenThrow(new IllegalArgumentException("there's already such subscription"));
    Mockito.when(this.dbService.getCustomerByID(this.initSub.customerId)).thenReturn(this.initCustomer);
    Mockito.when(this.dbService.getPlanByID(this.initSub.planId)).thenReturn(this.initPlan);

    this.subscriptionManager.createSubscription(this.initSub);
//    assertEquals(this.dbService.getCustomerByID(this.initCustomer.id).balance, 666 - 300);

    List<SubscriptionPojo> newSubs = this.subscriptionManager.getSubscriptions(this.initCustomer.id);
    for (SubscriptionPojo aSub : newSubs) {
      if (aSub.id == this.initSub.id) {
        break;
      }
      fail();
    }
  }

  @Test
  void createSubscriptionWithNullArgument() {
    Exception exception = assertThrows(IllegalArgumentException.class, () ->
        this.subscriptionManager.createSubscription(null));
    assertEquals("Argument 'subscription' is null", exception.getMessage());
  }

  @Test
  void createSubscriptionWithCorrectBalance() {
    Mockito.when(this.dbService.getSubscriptionByID(this.initSub.id))
            .thenThrow(new IllegalArgumentException("there's already such subscription"));
    Mockito.when(this.dbService.getCustomerByID(this.initSub.customerId)).thenReturn(this.initCustomer);
    Mockito.when(this.dbService.getPlanByID(this.initSub.planId)).thenReturn(this.initPlan);

    this.initPlan.fee = this.initCustomer.balance;
    this.subscriptionManager.createSubscription(this.initSub);
  }

  @Test
  void createSubscriptionWithBigBalance() {
    Mockito.when(this.dbService.getSubscriptionByID(this.initSub.id))
            .thenThrow(new IllegalArgumentException("there's already such subscription"));
    Mockito.when(this.dbService.getCustomerByID(this.initSub.customerId)).thenReturn(this.initCustomer);
    Mockito.when(this.dbService.getPlanByID(this.initSub.planId)).thenReturn(this.initPlan);

    this.initPlan.fee = this.initCustomer.balance + 1;

    try {
      this.subscriptionManager.createSubscription(this.initSub);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(),
          "customer doesn't have enough money for the plan");
    }
  }

  @Test
  void createSubscriptionWithDuplicateIDs() {
    when(this.dbService.getSubscriptionByID(this.initSub.id))
        .then(invocation -> {
          UUID id = invocation.getArgument(0);
          if (this.initSub.id.equals(id)) {
            return null;
          }
          throw new IllegalArgumentException("Subscription with id '" + id + " was not found");
        });

    try {
      this.subscriptionManager.createSubscription(this.initSub);
      fail();
    } catch (Exception exception) {
      assertEquals(exception.getMessage(), "there's already such subscription");
    }
  }


  @Test
  void removeSubscription() {
    UUID id = UUID.randomUUID();
    this.subscriptionManager.removeSubscription(id);

    verify(this.dbService, times(1)).removeSubscriptionByID(id);
    verify(this.dbService, times(1)).removeSubscriptionByID(any());

    verify(this.dbService, times(0)).getSubscriptions(any());
  }

  @Test
  void removeSubscriptionWithNullArgument() {
    Exception exception = assertThrows(IllegalArgumentException.class, () ->
        this.subscriptionManager.removeSubscription(null));
    assertEquals("Argument 'subscriptionId' is null", exception.getMessage());
  }
}
