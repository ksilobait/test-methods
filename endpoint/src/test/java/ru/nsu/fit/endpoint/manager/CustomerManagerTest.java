package ru.nsu.fit.endpoint.manager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.glassfish.jersey.internal.util.Producer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.Collections;
import java.util.UUID;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class CustomerManagerTest {
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo createCustomerInput;

    @BeforeEach
    void init() {
        // create stubs for the test's class
        this.dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);

        // create the test's class
        this.customerManager = new CustomerManager(this.dbService, logger);

        this.createCustomerInput = new CustomerPojo();
        this.createCustomerInput.firstName = "John";
        this.createCustomerInput.lastName = "Wick";
        this.createCustomerInput.login = "john@mail.ru";
        this.createCustomerInput.pass = "123!@#qweQWE";
        this.createCustomerInput.balance = 0;
    }

    @Test void testCreateCustomer() {
        CustomerPojo createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = this.createCustomerInput.firstName;
        createCustomerOutput.lastName = this.createCustomerInput.lastName;
        createCustomerOutput.login = this.createCustomerInput.login;
        createCustomerOutput.pass = this.createCustomerInput.pass;
        createCustomerOutput.balance = this.createCustomerInput.balance;

        when(this.dbService.createCustomer(this.createCustomerInput)).thenReturn(createCustomerOutput);
        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = this.customerManager.createCustomer(this.createCustomerInput);

        // Проверяем результат выполенния метода
        assertEquals(customer.id, createCustomerOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(this.dbService, times(1)).createCustomer(this.createCustomerInput);
        verify(this.dbService, times(1)).getCustomerIdByLogin(this.createCustomerInput.login);

        // Проверяем, что другие методы не вызывались...
        verify(this.dbService, times(0)).getCustomers(); // TODO need more?
    }

    // Используйте expected exception аннотации или expected exception rule...
    @Test void testCreateCustomerWithNullArgument() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                this.customerManager.createCustomer(null));
        assertEquals("Argument 'customerData' is null", exception.getMessage());
    }

    @Test void testCreateCustomerFirstNameLeftBound() {
        this.createCustomerInput.firstName = "Qq";
        assertEquals(this.createCustomerInput.firstName.length(), 2);

        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));
        this.customerManager.createCustomer(this.createCustomerInput);
    }

    @Test void testCreateCustomerFirstNameRightBound() {
        this.createCustomerInput.firstName = "Q" + String.join("", Collections.nCopies(11, "q"));
        assertEquals(this.createCustomerInput.firstName.length(), 12);

        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));
        this.customerManager.createCustomer(this.createCustomerInput);
    }

    @Test void testCreateCustomerLastNameLeftBound() {
        this.createCustomerInput.lastName = "Qq";
        assertEquals(this.createCustomerInput.lastName.length(), 2);

        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));
        this.customerManager.createCustomer(this.createCustomerInput);
    }

    @Test void testCreateCustomerLastNameRightBound() {
        this.createCustomerInput.lastName = "Q" + String.join("", Collections.nCopies(11, "q"));
        assertEquals(this.createCustomerInput.lastName.length(), 12);

        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));
        this.customerManager.createCustomer(this.createCustomerInput);
    }

    @Test void testCreateCustomerWithIncorrectFirstName() {
        this.testInteractWithCustomerWithIncorrectName(
                s -> this.createCustomerInput.firstName = s,
                () -> this.createCustomerInput.firstName,
                "FirstName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively",
                c -> this.customerManager.createCustomer(c)
        );
    }

    @Test void testCreateCustomerWithIncorrectLastName() {
        this.testInteractWithCustomerWithIncorrectName(
                s -> this.createCustomerInput.lastName = s,
                () -> this.createCustomerInput.lastName,
                "LastName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively",
                c -> this.customerManager.createCustomer(c)
        );
    }

    private void testInteractWithCustomerWithIncorrectName(Consumer<String> setter, Producer<String> getter, String mainExceptionMessage, Consumer<CustomerPojo> action) {

        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));

        Exception exception;

        setter.accept("Q");
        assertEquals(getter.call().length(), 1);
        exception = assertThrows(IllegalArgumentException.class, () -> action.accept(this.createCustomerInput));
        assertEquals(mainExceptionMessage, exception.getMessage());

        setter.accept("Q" + String.join("", Collections.nCopies(12, "q")));
        assertEquals(getter.call().length(), 13);
        exception = assertThrows(IllegalArgumentException.class, () -> action.accept(this.createCustomerInput));
        assertEquals(mainExceptionMessage, exception.getMessage());

        setter.accept("Q q");
        assertTrue(getter.call().contains(" "));
        exception = assertThrows(IllegalArgumentException.class, () -> action.accept(this.createCustomerInput));
        assertEquals(mainExceptionMessage, exception.getMessage());

        setter.accept("qwe");
        assertTrue(Character.isLowerCase(getter.call().charAt(0)));
        exception = assertThrows(IllegalArgumentException.class, () -> action.accept(this.createCustomerInput));
        assertEquals(mainExceptionMessage, exception.getMessage());

        setter.accept("Q123");
        exception = assertThrows(IllegalArgumentException.class, () -> action.accept(this.createCustomerInput));
        assertEquals(mainExceptionMessage, exception.getMessage());
    }

    @Test void testCreateCustomerWithIncorrectLogin() {
        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));

        Exception exception;

        this.createCustomerInput.login = "qwe@@abc.com";
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals("Login should be a valid email", exception.getMessage());
    }

    @Test void testCreateCustomerWithDupLogin() {
        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .then(invocation -> {
                    String login = (String)invocation.getArgument(0);
                    if (this.createCustomerInput.login.equalsIgnoreCase(login)) {
                        return null;
                    }
                    throw new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found");
                });

        Exception exception;

        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals("Login should be unique", exception.getMessage());
    }

    @Test void testCreateCustomerWithIncorrectPassword() {
        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));

        Exception exception;

        this.createCustomerInput.pass = String.join("", Collections.nCopies(5, "1"));
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols", exception.getMessage());

        this.createCustomerInput.pass = String.join("", Collections.nCopies(13, "1"));
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols", exception.getMessage());

        this.createCustomerInput.pass = "123qwe";
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals("Password is easy", exception.getMessage());

        this.createCustomerInput.pass = this.createCustomerInput.firstName + "qwe";
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals("Password should not contain first name", exception.getMessage());

        this.createCustomerInput.pass = this.createCustomerInput.lastName + "qwe";
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals("Password should not contain last name", exception.getMessage());

        this.createCustomerInput.pass = this.createCustomerInput.login;
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals("Password should not contain login", exception.getMessage());
    }

    @Test void testCreateCustomerWithNonZeroBalance() {
        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));

        Exception exception;
        String commonExceptionMessage = "Balance should be 0";

        this.createCustomerInput.balance = -1;
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals(commonExceptionMessage, exception.getMessage());

        this.createCustomerInput.balance = 1;
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.createCustomer(this.createCustomerInput));
        assertEquals(commonExceptionMessage, exception.getMessage());
    }

    @Test void testRemoveCustomerWithNullArgument() {
        Exception exception;
        String commonExceptionMessage = "Argument 'id' is null";

        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.removeCustomer(null));
        assertEquals(commonExceptionMessage, exception.getMessage());
    }

    @Test void testRemoveCustomer() {
        UUID id = UUID.randomUUID();
        this.customerManager.removeCustomer(id);

        verify(this.dbService, times(1)).deleteCustomerByID(id);
        verify(this.dbService, times(1)).deleteCustomerByID(any());

        verify(this.dbService, times(0)).getCustomers(); // TODO need more?
    }

    @Test void testRemoveCustomerNonexistent() {

//        when(this.dbService.createCustomer(this.createCustomerInput)).thenReturn(createCustomerOutput);
//        when(this.dbService.getCustomerIdByLogin(this.createCustomerInput.login))
//                .thenThrow(new IllegalArgumentException("Customer with login '" + this.createCustomerInput.login + " was not found"));

        this.createCustomerInput.id = UUID.randomUUID();
        Exception exception;
        UUID id = this.createCustomerInput.id;
        String commonExceptionMessage = "Customer with id " + id + " was not found"; // TODO

        doThrow(new IllegalArgumentException(commonExceptionMessage)).when(this.dbService).deleteCustomerByID(this.createCustomerInput.id);

        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.removeCustomer(id));
        assertEquals(commonExceptionMessage, exception.getMessage());
    }

    @Test void testUpdateCustomerWithNullArgument() {
        Exception exception;
        String commonExceptionMessage = "Argument 'customerData' is null";

        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.updateCustomer(null));
        assertEquals(commonExceptionMessage, exception.getMessage());
    }

    @Test void testUpdateCustomer() {
        CustomerPojo updateCustomerOutput = new CustomerPojo();
        updateCustomerOutput.id = this.createCustomerInput.id;
        updateCustomerOutput.firstName = this.createCustomerInput.firstName;
        updateCustomerOutput.lastName = this.createCustomerInput.lastName;
        updateCustomerOutput.login = this.createCustomerInput.login;
        updateCustomerOutput.pass = this.createCustomerInput.pass;
        updateCustomerOutput.balance = this.createCustomerInput.balance;

        CustomerPojo getCustomerOutput = new CustomerPojo();
        getCustomerOutput.id = this.createCustomerInput.id;
        getCustomerOutput.firstName = this.createCustomerInput.firstName;
        getCustomerOutput.lastName = this.createCustomerInput.lastName;
        getCustomerOutput.login = this.createCustomerInput.login;
        getCustomerOutput.pass = this.createCustomerInput.pass;
        getCustomerOutput.balance = this.createCustomerInput.balance;

        when(this.dbService.updateCustomer(this.createCustomerInput)).thenReturn(updateCustomerOutput);
        when(this.dbService.getCustomerByID(this.createCustomerInput.id)).thenReturn(getCustomerOutput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = this.customerManager.updateCustomer(this.createCustomerInput);

        assertEquals(customer, updateCustomerOutput);

        // Проверяем, что метод был вызван ровно 1 раз с определенными аргументами
        verify(this.dbService, times(1)).updateCustomer(this.createCustomerInput);

        // Проверяем, что другие методы не вызывались...
        verify(this.dbService, times(0)).getCustomers(); // TODO need more?
    }

    @Test void testUpdateCustomerNonexistent() {
        this.createCustomerInput.id = UUID.randomUUID();
        UUID id = this.createCustomerInput.id;
        String commonExceptionMessage = "Customer not found";

        when(this.dbService.updateCustomer(this.createCustomerInput)).thenThrow(new IllegalArgumentException(commonExceptionMessage));

        Exception exception;
        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.updateCustomer(this.createCustomerInput));
        assertEquals(commonExceptionMessage, exception.getMessage());
    }

    @Test void testUpdateCustomerWithIncorrectFirstName() {
        this.testInteractWithCustomerWithIncorrectName(
                s -> this.createCustomerInput.firstName = s,
                () -> this.createCustomerInput.firstName,
                "FirstName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively",
                c -> this.customerManager.updateCustomer(c)
        );
    }

    @Test void testUpdateCustomerWithIncorrectLastName() {
        this.testInteractWithCustomerWithIncorrectName(
                s -> this.createCustomerInput.lastName = s,
                () -> this.createCustomerInput.lastName,
                "LastName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively",
                c -> this.customerManager.updateCustomer(c)
        );
    }

    @Test void testTopUpBalance() {
        UUID id = UUID.randomUUID();
        this.customerManager.topUpBalance(id, 1);

        verify(this.dbService, times(1)).increaseCustomerBalance(id, 1);

        verify(this.dbService, times(0)).getCustomers(); // TODO need more?
    }

    @Test void testTopUpBalanceWithNullArgument() {
        Exception exception;
        String commonExceptionMessage = "Argument 'id' is null";

        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.topUpBalance(null, 1));
        assertEquals(commonExceptionMessage, exception.getMessage());
    }

    @Test void testTopUpBalanceWithNonExistentID() {
        UUID id = UUID.randomUUID();

        Exception exception;
        String commonExceptionMessage = "Customer with id '" + id + " was not found";

        when(this.dbService.increaseCustomerBalance(id, 1)).thenThrow(new IllegalArgumentException("Customer with id '" + id + " was not found"));

        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.topUpBalance(id, 1));
        assertEquals(commonExceptionMessage, exception.getMessage());
    }

    @Test void testTopUpBalanceWithIncorrectBalance() {
        UUID id = UUID.randomUUID();

        Exception exception;
        String commonExceptionMessage = "Amount should be strictly more than 0";

        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.topUpBalance(id, -1));
        assertEquals(commonExceptionMessage, exception.getMessage());

        exception = assertThrows(IllegalArgumentException.class, () -> this.customerManager.topUpBalance(id, 0));
        assertEquals(commonExceptionMessage, exception.getMessage());
    }
}
