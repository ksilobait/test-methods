$(document).ready(function(){
    $("#login").click(function(){
        var login = $("#email").val();
        var password = $("#password").val();
        // Checking for blank fields.
        if(login ==='' || password ==='') {
            $('input[type="text"],input[type="password"]').css("border","2px solid red");
            $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
            alert("Email or password is empty");
        } else {
            $.get({
                url: 'rest/get_role',
                headers: {
                    'Authorization': 'Basic ' + btoa(login + ':' + password)
                }
            }).done(function(data) {
                var dataObj = $.parseJSON(data);
                if(dataObj['role']=='ADMIN') {
                    $.redirect('/endpoint/customers.html', {'login': login, 'pass': password, 'role': 'ADMIN'}, 'GET');
                } else if(dataObj['role']=='UNKNOWN') {
                    $('input[type="text"],input[type="password"]').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
                    alert("Email or password is incorrect");
                } else if(dataObj['role']=='CUSTOMER') {
                    $.get({
                        url: 'rest/get_customer_by_login/' + login,
                        headers: {
                            'Authorization': 'Basic ' + btoa(login + ':' + password)
                        }
                    }).done(function(data) {
//                         console.log(data);
                        $.redirect('/endpoint/subscriptions.html', {
                            'login': data['login'],
                            'customerId': data['id']
                        }, 'GET');
                    });

//                     $.redirect('/endpoint/subscriptions.html', {'login': login, 'role': 'CUSTOMER'}, 'GET');
                }
            });
        }
    });
});
