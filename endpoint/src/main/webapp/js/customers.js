$(document).ready(function(){
    $("#add_new_customer").click(function() {
        $.redirect('/endpoint/add_customer.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });

    $("#plans").click(function() {
        $.redirect('/endpoint/plans.html',
            {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'},
            'GET')
    });

    $.get({
        url: 'rest/get_customers',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function(data) {
        var dataSet = [];
        for(var i = 0; i < data.length; i++) {
            var obj = data[i];
            dataSet.push([obj.firstName, obj.lastName, obj.login, obj.pass, obj.balance])
        }

        let updateTable = function() {
            return $('#customer_list_id')
                .DataTable({
                    data: dataSet,
                    columns: [
                        { title: "Fist Name" },
                        { title: "Last Name" },
                        { title: "Email" },
                        { title: "Pass" },
                        { title: "Balance" },
                        { title: "Increase balance" }
                    ],
                    "columnDefs": [ {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<button>Increase by 100</button>"
                    } ]
                });
        }

        let table = updateTable();

        $('#customer_list_id tbody').on( 'click', 'button', function () {
            let increaseAmount = 100;
            let rowNumber = $(this).parents('tr');
            let data = table.row(rowNumber).data();
            let login = data[2];
            $.post({
                url: 'rest/increase_customer_balance_by_login/' + login + '/' + increaseAmount,
                headers: {
                    'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                    'Content-Type': 'application/json'
                },
            }).done(function(data) {
                data = table.row(rowNumber).data();
                data[4] = data[4] + increaseAmount;
                $('#customer_list_id').dataTable().fnUpdate(data, rowNumber, undefined, false);
            }).fail(function(xhr, status, error){
                let firstLine = xhr.responseText.split('\n')[0];
                alert(firstLine);
            });
        });
    });
});
