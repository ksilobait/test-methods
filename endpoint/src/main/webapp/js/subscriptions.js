$(document).ready(function () {
	var login = $.parseUrl(
		decodeURIComponent(window.location.href)).params.login;

	var plans = {}
	$.get({
		url: 'rest/get_plans/',
		headers: {
			'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
		}
	}).done(function (data) {
		for (var i = 0; i < data.length; i++) {
			let obj = data[i];
			plans[obj.id] = {};
			plans[obj.id]['name'] = obj.name;
			plans[obj.id]['fee'] = obj.fee;
		}
	});

	$.get({
		url: 'rest/get_customer_subscriptions/' + login,
		headers: {
			'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
		}
	}).done(function (data) {
		let dataSet = [];
		for (var i = 0; i < data.length; i++) {
			let obj = data[i];
			dataSet.push([obj.id, plans[obj.planId]['name'], plans[obj.planId]['fee']])
		}

		let table = $('#subscription_list_id').DataTable({
			data: dataSet,
			columns: [
				{title: "Subscription ID"},
				{title: "Plan"},
				{title: "Fee"},
				{title: "Unsubscribe"},
			],
			"columnDefs": [
				{
					"targets": [ 0 ],
					"visible": false,
					"searchable": false
				},
				{
					"targets": -1,
					"data": null,
					"defaultContent": "<button>Unsubscribe</button>"
				}
			]
		});


		$('#subscription_list_id tbody').on( 'click', 'button', function () {
			let rowNumber = $(this).parents('tr');
			let data = table.row(rowNumber).data();
			let subIId = data[0];
			$.post({
				url: 'rest/unsubscribe/' + subIId,
				headers: {
					'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
					'Content-Type': 'application/json'
				},
			}).done(function() {
				data = table.row(rowNumber).data();
				data[2] = 'Unsubbed';
				$('#subscription_list_id').dataTable().fnUpdate(data, rowNumber, undefined, false);
			}).fail(function(xhr, status, error){
				let firstLine = xhr.responseText.split('\n')[0];
				alert(firstLine);
			});
		});
	});
});
