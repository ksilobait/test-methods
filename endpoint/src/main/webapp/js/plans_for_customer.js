$(document).ready(function () {
	var login = $.parseUrl(
		decodeURIComponent(window.location.href)).params.login;

	$.get({
		url: 'rest/get_plans_for_customer_by_login/' + login,
		headers: {
			'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
		}
	}).done(function (data) {
		var dataSet = [];
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			dataSet.push([obj.name, obj.details, obj.fee])
		}

		let table = $('#plan_list_id')
		.DataTable({
			data: dataSet,
			columns: [
				{title: "Name"},
				{title: "Details"},
				{title: "Fee"},
				{title: "Subscribe"},
			],
			"columnDefs": [ {
				"targets": -1,
				"data": null,
				"defaultContent": "<button>Subscribe</button>"
			} ]
		});

		$('#plan_list_id tbody').on( 'click', 'button', function () {
			let rowNumber = $(this).parents('tr');
			let data = table.row(rowNumber).data();
			let planName = data[0];
			$.post({
				url: 'rest/subscribe/' + login + '/' + planName,
				headers: {
					'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
					'Content-Type': 'application/json'
				},
			}).done(function() {
				data = table.row(rowNumber).data();
				data[3] = 'Subscribed';
				$('#plan_list_id').dataTable().fnUpdate(data, rowNumber, undefined, false);
			}).fail(function(xhr, status, error){
				let firstLine = xhr.responseText.split('\n')[0];
				alert(firstLine);
			});
		});
	});
});
