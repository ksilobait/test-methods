$(document).ready(function () {
	$("#customers").click(function() {
		$.redirect('/endpoint/customers.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
	});

	$("#add_new_plan").click(function () {
		$.redirect('/endpoint/add_plan.html',
			{'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
	});

	$.get({
		url: 'rest/get_plans',
		headers: {
			'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
		}
	}).done(function (data) {
		var dataSet = [];
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			dataSet.push([obj.name, obj.details, obj.fee])
		}

		$('#plan_list_id')
		.DataTable({
			data: dataSet,
			columns: [
				{title: "Name"},
				{title: "Details"},
				{title: "Fee"}
			]
		});
	});
});
