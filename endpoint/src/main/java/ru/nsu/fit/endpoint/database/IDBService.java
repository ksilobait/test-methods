package ru.nsu.fit.endpoint.database;

import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.List;
import java.util.UUID;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;

public interface IDBService {
    CustomerPojo createCustomer(CustomerPojo customerData);

    CustomerPojo updateCustomer(CustomerPojo customerData);

    public void deleteCustomerByID(UUID id);

    public CustomerPojo increaseCustomerBalance(UUID id, int amount);

    UUID getCustomerIdByLogin(String customerLogin);

    public CustomerPojo getCustomerByID(UUID id);

    List<CustomerPojo> getCustomers();


    PlanPojo createPlan(PlanPojo plan);

    PlanPojo updatePlan(PlanPojo plan);

    void removePlanByID(UUID id);

    UUID getPlanIDByName(String planName);

    PlanPojo getPlanByID(UUID id);

    List<PlanPojo> getPlans();


    SubscriptionPojo createSubscription(SubscriptionPojo subscription, int newBalance);

    void removeSubscriptionByID(UUID subscriptionId);

    SubscriptionPojo getSubscriptionByID(UUID id);

    List<SubscriptionPojo> getSubscriptions(UUID customerId);
}
