package ru.nsu.fit.endpoint.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class DBService implements IDBService{
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, balance) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id) values ('%s', '%s', '%s')";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, fee) values ('%s', '%s', '%s', %s)";

    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";
    private static final String SELECT_CUSTOMER_BY_ID = "SELECT * FROM CUSTOMER WHERE id='%s'";

    private static final String UPDATE_CUSTOMER_BY_ID = "UPDATE CUSTOMER SET first_name = '%s', last_name = '%s', login = '%s', pass = '%s' WHERE id='%s'";
    private static final String DELETE_CUSTOMER_BY_ID = "DELETE FROM CUSTOMER WHERE id='%s'";
    private static final String INCREASE_CUSTOMER_BALANCE = "UPDATE CUSTOMER SET balance = '%s' WHERE id='%s'";

    private static final String SELECT_PLANS = "SELECT * FROM PLAN";
    private static final String SELECT_PLAN_ID_BY_NAME = "SELECT id FROM PLAN WHERE name='%s'";
    private static final String SELECT_PLAN_BY_ID = "SELECT * FROM PLAN WHERE id='%s'";
    private static final String UPDATE_PLAN_BY_ID = "UPDATE PLAN SET name = '%s', details = '%s', fee = '%s' WHERE id='%s'";
    private static final String REMOVE_PLAN_BY_ID = "DELETE FROM PLAN WHERE id='%s'";

    private static final String SELECT_SUBSCRIPTION_BY_ID = "SELECT * FROM SUBSCRIPTION WHERE id='%s'";
    private static final String REMOVE_SUBSCRIPTION_BY_ID = "DELETE FROM SUBSCRIPTION WHERE id='%s'";
    private static final String SELECT_SUBSCRIPTIONS_BY_ID = "SELECT * FROM SUBSCRIPTION WHERE customer_id='%s'";

    private static final String CLEAN_CUSTOMERS = "DELETE FROM CUSTOMER";
    private static final String CLEAN_PLANS = "DELETE FROM PLAN";
    private static final String CLEAN_SUBSCRIPTIONS = "DELETE FROM SUBSCRIPTION";

    private Logger logger;
    private static final Object generalMutex = new Object();
    private Connection connection;

    public DBService(Logger logger) {
        this.logger = logger;
        this.init();
    }

    public CustomerPojo createCustomer(CustomerPojo customerData) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'createCustomer' was called with data: \n%s", JsonMapper.toJson(customerData, true)));

            customerData.id = UUID.randomUUID();
            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customerData.id,
                                customerData.firstName,
                                customerData.lastName,
                                customerData.login,
                                customerData.pass,
                                customerData.balance));
                return customerData;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public CustomerPojo updateCustomer(CustomerPojo customerData) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'updateCustomer' was called with data: \n%s", JsonMapper.toJson(customerData, true)));

            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_CUSTOMER_BY_ID,
                                customerData.firstName,
                                customerData.lastName,
                                customerData.login,
                                customerData.pass,
                                customerData.id));
                return customerData;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public PlanPojo updatePlan(PlanPojo plan) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'updatePlan' was called with data: \n%s", JsonMapper.toJson(plan, true)));

            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate(
                    String.format(
                        UPDATE_PLAN_BY_ID,
                        plan.name.replace("'", "\\'"),
                        plan.details.replace("'", "\\'"),
                        plan.fee,
                        plan.id));
                return plan;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public CustomerPojo increaseCustomerBalance(UUID id, int amount) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'increaseCustomerBalance' was called with data: \n%s, %s", id, amount));

            CustomerPojo customerData = new CustomerPojo();
            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_BY_ID,
                                id));
                if (rs.next()) {
                    customerData.firstName = rs.getString(2);
                    customerData.lastName = rs.getString(3);
                    customerData.login = rs.getString(4);
                    customerData.pass = rs.getString(5);
                    customerData.balance = rs.getInt(6);
                } else {
                    throw new IllegalArgumentException("Customer with id '" + id + " was not found");
                }

                statement = this.connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INCREASE_CUSTOMER_BALANCE,
                                customerData.balance + amount,
                                id));

                return customerData;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void deleteCustomerByID(UUID id) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'deleteCustomerByID' was called with data '%s'.", id));

            try {
                Statement statement = this.connection.createStatement();
                int rowCount = statement.executeUpdate(
                        String.format(
                                DELETE_CUSTOMER_BY_ID,
                                id));
                if (rowCount == 0) {
                    throw new IllegalArgumentException("Customer with id '" + id + " was not found");
                }
                if (rowCount > 1) {
                    throw new RuntimeException("Unexpected result: " + rowCount + " rows with id '" + id);
                }
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void cleanDatabase() {
        synchronized (generalMutex) {
            this.logger.info("Method 'cleanDatabase' was called");

            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate(CLEAN_CUSTOMERS);
                statement.executeUpdate(CLEAN_PLANS);
                statement.executeUpdate(CLEAN_SUBSCRIPTIONS);
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void removePlanByID(UUID id) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'removePlanByID' was called with data '%s'.", id));

            try {
                Statement statement = this.connection.createStatement();
                int rowCount = statement.executeUpdate(
                    String.format(
                        REMOVE_PLAN_BY_ID,
                        id));
                if (rowCount == 0) {
                    throw new IllegalArgumentException("Plan with id '" + id + " was not found");
                }
                if (rowCount > 1) {
                    throw new IllegalArgumentException("unexpected rowCount " + rowCount + "for plan with id " + id);
                }
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void removeSubscriptionByID(UUID id) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'removeSubscriptionByID' was called with data '%s'.", id));

            try {
                Statement statement = this.connection.createStatement();
                int rowCount = statement.executeUpdate(
                    String.format(
                        REMOVE_SUBSCRIPTION_BY_ID,
                        id));
                if (rowCount == 0) {
                    throw new IllegalArgumentException("Subscription with id '" + id + " was not found");
                }
                if (rowCount > 1) {
                    throw new IllegalArgumentException("unexpected rowCount " + rowCount + "for plan with id " + id);
                }
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public List<CustomerPojo> getCustomers() {
        synchronized (generalMutex) {
            this.logger.info("Method 'getCustomers' was called.");

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<CustomerPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    CustomerPojo customerData = new CustomerPojo();

                    customerData.id = UUID.fromString(rs.getString(1));
                    customerData.firstName = rs.getString(2);
                    customerData.lastName = rs.getString(3);
                    customerData.login = rs.getString(4);
                    customerData.pass = rs.getString(5);
                    customerData.balance = rs.getInt(6);

                    result.add(customerData);
                }
                return result;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public List<PlanPojo> getPlans() {
        synchronized (generalMutex) {
            this.logger.info("Method 'getPlans' was called.");

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_PLANS);
                List<PlanPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    PlanPojo plan = new PlanPojo();
                    plan.id = UUID.fromString(rs.getString(1));
                    plan.name = rs.getString(2);
                    plan.details = rs.getString(3);
                    plan.fee = rs.getInt(4);

                    result.add(plan);
                }
                return result;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public List<SubscriptionPojo> getSubscriptions(UUID customerId) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'getSubscriptions' was called with data '%s'.", customerId));

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                    String.format(
                        SELECT_SUBSCRIPTIONS_BY_ID,
                        customerId));
                List<SubscriptionPojo> result = Lists.newArrayList();
                while (rs.next()) {
                    SubscriptionPojo subscription = new SubscriptionPojo();
                    subscription.id = UUID.fromString(rs.getString(1));
                    subscription.customerId = UUID.fromString(rs.getString(2));
                    subscription.planId = UUID.fromString(rs.getString(3));
                    result.add(subscription);
                }
                return result;
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'getCustomerIdByLogin' was called with data '%s'.", customerLogin));

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (!rs.next()) {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
                return UUID.fromString(rs.getString(1));
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public UUID getPlanIDByName(String planName) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'getPlanIDByName' was called with data '%s'.", planName));

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                    String.format(
                        SELECT_PLAN_ID_BY_NAME,
                        planName));
                if (!rs.next()) {
                    throw new IllegalArgumentException("Plan with name '" + planName + " was not found");
                }
                return UUID.fromString(rs.getString(1));
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public CustomerPojo getCustomerByID(UUID id) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'getCustomerByID' was called with data '%s'.", id));

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_BY_ID,
                                id));
                if (!rs.next()) {
                    throw new IllegalArgumentException("Customer with id '" + id + " was not found");
                }

                CustomerPojo customerData = new CustomerPojo();

                customerData.firstName = rs.getString(2);
                customerData.lastName = rs.getString(3);
                customerData.login = rs.getString(4);
                customerData.pass = rs.getString(5);
                customerData.balance = rs.getInt(6);

                return customerData;
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public PlanPojo getPlanByID(UUID id) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'getPlanByID' was called with data '%s'.", id));

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                    String.format(
                        SELECT_PLAN_BY_ID,
                        id));
                if (!rs.next()) {
                    throw new IllegalArgumentException("Plan with id '" + id + " was not found");
                }

                PlanPojo planData = new PlanPojo();
                planData.id = id;
                planData.name = rs.getString(2);
                planData.details = rs.getString(3);
                planData.fee = rs.getInt(4);
                return planData;
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public SubscriptionPojo getSubscriptionByID(UUID id) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'getSubscriptionByID' was called with data '%s'.", id));

            try {
                Statement statement = this.connection.createStatement();
                ResultSet rs = statement.executeQuery(
                    String.format(
                        SELECT_SUBSCRIPTION_BY_ID,
                        id));
                if (!rs.next()) {
                    throw new IllegalArgumentException("Subscription with id '" + id + " was not found");
                }

                SubscriptionPojo subscription = new SubscriptionPojo();
                subscription.customerId = UUID.fromString(rs.getString(2));
                subscription.planId = UUID.fromString(rs.getString(3));
                return subscription;
            } catch (SQLException ex) {
                this.logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public PlanPojo createPlan(PlanPojo plan) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'createPlan' was called with data '%s'.", JsonMapper.toJson(plan, true)));

            plan.id = UUID.randomUUID();
            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_PLAN,
                                plan.id,
                                plan.name.replace("'", "\\'"),
                                plan.details.replace("'", "\\'"),
                                plan.fee));
                return plan;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public SubscriptionPojo createSubscription(SubscriptionPojo subscription, int newBalance) {
        synchronized (generalMutex) {
            this.logger.info(String.format("Method 'createSubscription' was called with data '%s'.", JsonMapper.toJson(subscription, true)));
            subscription.id = UUID.randomUUID();
            try {
                Statement statement = this.connection.createStatement();
                statement.executeUpdate(
                    String.format(
                        INSERT_SUBSCRIPTION,
                        subscription.id,
                        subscription.customerId,
                        subscription.planId));

                statement = this.connection.createStatement();
                statement.executeUpdate(
                    String.format(
                        INCREASE_CUSTOMER_BALANCE,
                        newBalance,
                        subscription.customerId));
                return subscription;
            } catch (SQLException ex) {
                this.logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private void init() {
        this.logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            this.logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        this.logger.debug("MySQL JDBC Driver Registered!");

        try {
            this.connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            this.logger.error("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (this.connection != null) {
            this.logger.debug("You made it, take control your database now!");
        } else {
            this.logger.error("Failed to make connection!");
        }
    }
}
