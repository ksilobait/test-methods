package ru.nsu.fit.endpoint.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.MainFactory;
import ru.nsu.fit.endpoint.database.data.CustomPlanPojo;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.HealthCheckPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.endpoint.database.data.RolePojo;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;
import ru.nsu.fit.endpoint.manager.CustomerManager;
import ru.nsu.fit.endpoint.manager.SubscriptionManager;
import ru.nsu.fit.endpoint.shared.Globals;
import ru.nsu.fit.endpoint.shared.JsonMapper;

@Path("")
public class RestService {
    @RolesAllowed({Globals.UNKNOWN_ROLE, Globals.ADMIN_ROLE, Globals.CUSTOMER_ROLE})
    @GET
    @Path("/health_check")
    public Response healthCheck() {
        return Response.ok().entity(JsonMapper.toJson(new HealthCheckPojo(), true)).build();
    }

    @RolesAllowed({Globals.UNKNOWN_ROLE, Globals.ADMIN_ROLE, Globals.CUSTOMER_ROLE})
    @GET
    @Path("/get_role")
    public Response getRole(@Context ContainerRequestContext crc) {
        RolePojo rolePojo = new RolePojo();
        rolePojo.role = crc.getProperty("ROLE").toString();

        return Response.ok().entity(JsonMapper.toJson(rolePojo, true)).build();
    }

    // Example request: ../customers?login='john_wick@gmail.com'
    @RolesAllowed(Globals.ADMIN_ROLE)
    @GET
    @Path("/get_customers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomers(@DefaultValue("") @QueryParam("login") String customerLogin) {
        try {
            List<CustomerPojo> customers = MainFactory.getInstance()
                    .getCustomerManager()
                    .getCustomers().stream()
                    .filter(x -> customerLogin.isEmpty() || x.login.equals(customerLogin))
                    .collect(Collectors.toList());

            return Response.ok().entity(JsonMapper.toJson(customers, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        try {
            // convert json to object
            CustomerPojo customerData = JsonMapper.fromJson(customerDataJson, CustomerPojo.class);

            // create new customer
            CustomerPojo customer = MainFactory.getInstance().getCustomerManager().createCustomer(customerData);

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    private Optional<CustomerPojo> findCustomer(String login) {
        List<CustomerPojo> allCustomers = MainFactory.getInstance().getCustomerManager().getCustomers();
        return allCustomers.stream().filter(x -> x.login.equals(login)).findFirst();
    }

    private Optional<PlanPojo> findPlan(String name) {
        List<PlanPojo> allCustomers = MainFactory.getInstance().getPlanManager().getAllPlans();
        return allCustomers.stream().filter(x -> x.name.equals(name)).findFirst();
    }

    @RolesAllowed({Globals.ADMIN_ROLE, Globals.CUSTOMER_ROLE})
    @GET
    @Path("/get_customer_by_login/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            Optional<CustomerPojo> customerOpt = this.findCustomer(customerLogin);
            if (!customerOpt.isPresent()) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            return Response.ok().entity(JsonMapper.toJson(customerOpt.get(), true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE, Globals.CUSTOMER_ROLE})
    @GET
    @Path("/get_customer_subscriptions/{customer_login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerSubscriptions(@PathParam("customer_login") String customerLogin) {
        try {
            Optional<CustomerPojo> customerOpt = this.findCustomer(customerLogin);
            if (!customerOpt.isPresent()) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SubscriptionManager manager = MainFactory.getInstance().getSubscriptionManager();
            List<SubscriptionPojo> subs = manager.getSubscriptions(customerOpt.get().id);

            return Response.ok().entity(JsonMapper.toJson(subs, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE, Globals.CUSTOMER_ROLE})
    @POST
    @Path("/subscribe/{customer_login}/{plan_name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribe(@PathParam("customer_login") String customerLogin, @PathParam("plan_name") String planName) {
        try {
            Optional<CustomerPojo> customerOpt = this.findCustomer(customerLogin);
            if (!customerOpt.isPresent()) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            Optional<PlanPojo> planOpt = this.findPlan(planName);
            if (!planOpt.isPresent()) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SubscriptionManager manager = MainFactory.getInstance().getSubscriptionManager();
            SubscriptionPojo sub = new SubscriptionPojo();
            sub.customerId = customerOpt.get().id;
            sub.planId = planOpt.get().id;
            sub = manager.createSubscription(sub);

            return Response.ok().entity(JsonMapper.toJson(sub, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({Globals.ADMIN_ROLE, Globals.CUSTOMER_ROLE})
    @POST
    @Path("/unsubscribe/{subscription_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response unsubscribe(@PathParam("subscription_id") String subscriptionId) {
        try {
            MainFactory.getInstance().getSubscriptionManager().removeSubscription(UUID.fromString(subscriptionId));
            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/update_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCustomer(String customerDataJson) {
        try {
            // convert json to object
            CustomerPojo customerData = JsonMapper.fromJson(customerDataJson, CustomerPojo.class);

            // create new customer
            CustomerPojo customer = MainFactory.getInstance().getCustomerManager().updateCustomer(customerData);

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/remove_customer_by_login/{customer_login}")
    public Response removeCustomer(@PathParam("customer_login") String customerLogin) {
        try {
            List<CustomerPojo> allCustomers = MainFactory.getInstance().getCustomerManager().getCustomers();
            CustomerPojo customer = allCustomers.stream().filter(x -> x.login.equals(customerLogin)).collect(Collectors.toList()).get(0);
            System.out.println(customer.login);
            System.out.println(customer.id);
            MainFactory.getInstance().getCustomerManager().removeCustomer(customer.id);

            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/increase_customer_balance_by_login/{customer_login}/{amount}")
    public Response increaseBalance(@PathParam("customer_login") String customerLogin, @PathParam("amount") int amount) {
        try {
            List<CustomerPojo> allCustomers = MainFactory.getInstance().getCustomerManager().getCustomers();
            CustomerPojo customer = allCustomers.stream().filter(x -> x.login.equals(customerLogin)).collect(Collectors.toList()).get(0);
            System.out.println(customer.login);
            System.out.println(customer.id);
            MainFactory.getInstance().getCustomerManager().topUpBalance(customer.id, amount);

            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/create_plan")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPlan(String planDataJson) {
        try {
            // convert json to object
            CustomPlanPojo customPlanData = JsonMapper.fromJson(planDataJson, CustomPlanPojo.class);
            PlanPojo planData = new PlanPojo();
            planData.id = customPlanData.id;
            planData.name = customPlanData.name;
            planData.details = customPlanData.details;
            try {
                planData.fee = Integer.parseInt(customPlanData.fee);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("value of fee is not a correct number");
            }

            // create new customer
            PlanPojo customer = MainFactory.getInstance().getPlanManager().createPlan(planData);

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @GET
    @Path("/get_plans")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlans() {
        try {
            List<PlanPojo> plans = new ArrayList<>(MainFactory.getInstance().getPlanManager().getAllPlans());
            return Response.ok().entity(JsonMapper.toJson(plans, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }


    // Example request: ../customers?login='john_wick@gmail.com'
    @RolesAllowed(Globals.ADMIN_ROLE)
    @GET
    @Path("/get_plans_for_customer_by_login/{customer_login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlansForCustomerByLogin(@PathParam("customer_login") String customerLogin) {
        try {
            Optional<CustomerPojo> customerOpt = MainFactory.getInstance()
                    .getCustomerManager()
                    .getCustomers().stream()
                    .filter(x -> x.login.equals(customerLogin))
                    .findFirst();
            if (!customerOpt.isPresent()) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            List<PlanPojo> plans = MainFactory.getInstance().getPlanManager().getPlans(customerOpt.get().id);

            return Response.ok().entity(JsonMapper.toJson(plans, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }


    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/update_plan")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatePlan(String customerDataJson) {
        try {
            // convert json to object
            PlanPojo data = JsonMapper.fromJson(customerDataJson, PlanPojo.class);

            // create new customer
            PlanPojo plan = MainFactory.getInstance().getPlanManager().updatePlan(data);

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(plan, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }


    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/remove_plan_by_id/{planId}")
    public Response removePlan(@PathParam("planId") String planId) {
        try {
             MainFactory.getInstance().getPlanManager().removePlan(UUID.fromString(planId));

            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
}
