package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.Validate;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.List;
import java.util.UUID;

public class CustomerManager extends ParentManager {
    public CustomerManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */
    public CustomerPojo createCustomer(CustomerPojo customer) {
        Validate.notNull(customer, "Argument 'customerData' is null");

        Validate.notNull(customer.firstName);
        Validate.isTrue(customer.firstName.matches("^\\p{Lu}\\p{Ll}{1,11}$"), "FirstName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively");

        Validate.notNull(customer.lastName);
        Validate.isTrue(customer.lastName.matches("^\\p{Lu}\\p{Ll}{1,11}$"), "LastName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively");

        Validate.notNull(customer.login);
        Validate.isTrue(EmailValidator.getInstance().isValid(customer.login), "Login should be a valid email");
        boolean loginUnique = true;
        try {
            this.dbService.getCustomerIdByLogin(customer.login);
            loginUnique = false;
        } catch (IllegalArgumentException ignored) {
        }
        Validate.isTrue(loginUnique, "Login should be unique");

        Validate.notNull(customer.pass);
        Validate.isTrue(customer.pass.length() >= 6 && customer.pass.length() <= 12, "Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        Validate.isTrue(!customer.pass.equalsIgnoreCase("123qwe"), "Password is easy");
        Validate.isTrue(!customer.pass.equalsIgnoreCase("1q2w3e"), "Password is easy");
        Validate.isTrue(!customer.pass.contains(customer.firstName), "Password should not contain first name");
        Validate.isTrue(!customer.pass.contains(customer.lastName), "Password should not contain last name");
        Validate.isTrue(!customer.pass.contains(customer.login), "Password should not contain login");

        Validate.isTrue(customer.balance == 0, "Balance should be 0");

        return this.dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<CustomerPojo> getCustomers() {
        return this.dbService.getCustomers();
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public CustomerPojo updateCustomer(CustomerPojo customer) {
        Validate.notNull(customer, "Argument 'customerData' is null");

        Validate.notNull(customer.firstName);
        Validate.isTrue(customer.firstName.matches("^\\p{Lu}\\p{Ll}{1,11}$"), "FirstName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively");

        Validate.notNull(customer.lastName);
        Validate.isTrue(customer.lastName.matches("^\\p{Lu}\\p{Ll}{1,11}$"), "LastName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively");

        CustomerPojo original = null;
        try {
            original = this.dbService.getCustomerByID(customer.id);
        } catch (IllegalArgumentException ignored) { }
        Validate.notNull(original, "Customer not found");

        Validate.isTrue(customer.login.equals(original.login));
        Validate.isTrue(customer.pass.equals(original.pass));
        // Money should not be checked, because they can potentially change

        return this.dbService.updateCustomer(customer);
    }

    public void removeCustomer(UUID id) {
        Validate.notNull(id, "Argument 'id' is null");
        this.dbService.deleteCustomerByID(id);
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public CustomerPojo topUpBalance(UUID customerId, int amount) {
        Validate.notNull(customerId, "Argument 'id' is null");
        Validate.isTrue(amount > 0, "Amount should be strictly more than 0");
        return this.dbService.increaseCustomerBalance(customerId, amount);
    }
}
