package ru.nsu.fit.endpoint.manager;

import java.util.ArrayList;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.List;
import java.util.UUID;
import ru.nsu.fit.endpoint.database.data.SubscriptionPojo;

public class PlanManager extends ParentManager {
    public PlanManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1 включительно;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public PlanPojo createPlan(PlanPojo plan) {
        Validate.notNull(plan, "'plan' is null");

        Validate.notNull(plan.name, "'plan.name' is null");
        Validate.isTrue(plan.name.matches("^[a-zA-Z0-9 ]*$"), "name has special symbols");
        Validate.isTrue(plan.name.length() >= 1 && plan.name.length() <= 128, "the length of the name should be between 1 and 128 symbols");
        boolean isNameUnique = true;
        try {
            this.dbService.getPlanIDByName(plan.name);
            isNameUnique = false;
        } catch (IllegalArgumentException ignored) {
        }
        Validate.isTrue(isNameUnique, "the name should be unique");

        Validate.notNull(plan.details, "'plan.details' is null");
        Validate.isTrue(plan.details.length() >= 1 && plan.details.length() <= 1024, "the length of the details should be between 1 and 1024 symbols");

        Validate.isTrue(plan.fee >= 0 && plan.fee <= 999_999, "the fee should be between 0 and 999999");

        return this.dbService.createPlan(plan);
    }

    public PlanPojo updatePlan(PlanPojo plan) {
        Validate.notNull(plan, "'plan' is null");

        PlanPojo thePlan = this.dbService.getPlanByID(plan.id);
        Validate.notNull(thePlan, "there's no such plan");

        Validate.isTrue(plan.name.matches("^[a-zA-Z0-9 ]*$"), "plan's name has special symbols");
        Validate.isTrue(plan.name.length() >= 1 && plan.name.length() <= 128, "the length of the plan's name should be between 1 and 128 symbols");

        Validate.notNull(plan.details, "'plan.details' is null");
        Validate.isTrue(plan.details.length() >= 1 && plan.details.length() <= 1024, "the length of the plan's details should be between 1 and 1024 symbols");

        Validate.isTrue(plan.fee >= 0 && plan.fee <= 999_999, "the plan's fee should be between 0 and 999999");

        return this.dbService.updatePlan(plan);
    }

    public void removePlan(UUID id) {
        Validate.notNull(id, "Argument 'id' is null");
        this.dbService.removePlanByID(id);
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<PlanPojo> getPlans(UUID customerId) {
        List<PlanPojo> allPlans = this.dbService.getPlans();

        List<SubscriptionPojo> userSubscriptions = this.dbService.getSubscriptions(customerId);
        List<PlanPojo> userPlans = new ArrayList<>();
        for (SubscriptionPojo sub : userSubscriptions) {
            userPlans.add(this.dbService.getPlanByID(sub.planId));
        }

        allPlans.removeAll(userPlans);
        int customerBalance = this.dbService.getCustomerByID(customerId).balance;
        allPlans.removeIf(plan -> (plan.fee > customerBalance));
        return allPlans;
    }

    public List<PlanPojo> getAllPlans() {
        return this.dbService.getPlans();
    }
}
