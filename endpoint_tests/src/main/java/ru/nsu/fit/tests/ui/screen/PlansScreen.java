package ru.nsu.fit.tests.ui.screen;

import java.net.URI;
import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

public class PlansScreen {
    private static final String url = "/endpoint/plans.html";
    private By addNewPlanButton;
    private Browser browser;

    public PlansScreen(Browser browser) {
        this.browser = browser;

        browser.waitForElement(By.id("add_new_plan"));
        addNewPlanButton = By.id("add_new_plan");

        this.checkUrl();
    }

    private void checkUrl() {
        URI u = browser.getCurrentPageUrl();
        String path = u.getPath();
        if (!url.equals(path)) {
            throw new RuntimeException("wrong url: " + path);
        }
    }

    public void clickAddNewPlan() {
        browser.click(addNewPlanButton);
    }

    public String getDataFromTable(int column) {
        return browser.getDataFromTable(column);
    }

}
