package ru.nsu.fit.tests.ui.screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

import java.net.URI;

import static ru.nsu.fit.endpoint.shared.Globals.ADMIN_LOGIN;
import static ru.nsu.fit.endpoint.shared.Globals.ADMIN_PASS;

public class LoginScreen {
    private static final String url = "/endpoint/login.html";
    private final By emailField;
    private final By passwordField;
    private final By loginButton;
    private final Browser browser;

    public LoginScreen(Browser browser) {
        this.browser = browser;

        emailField = By.id("email");

        this.checkUrl();

        passwordField = By.id("password");
        loginButton = By.id("login");
    }

    public static CustomersScreen enterAsAdmin(Browser browser) {
        LoginScreen loginScreen = new LoginScreen(browser);
        loginScreen.enterEmail(ADMIN_LOGIN);
        loginScreen.enterPassword(ADMIN_PASS);
        loginScreen.clickLogin();

        return new CustomersScreen(browser);
    }

    private void checkUrl() {
        URI u = browser.getCurrentPageUrl();
        String path = u.getPath();
        if (!url.equals(path)) {
            throw new RuntimeException("wrong url: " + path);
        }
    }

    public void enterEmail(String email) {
        browser.typeText(emailField, email);
    }

    public void enterPassword(String password) {
        browser.typeText(passwordField, password);
    }

    public void clickLogin() {
        browser.click(loginButton);
    }
}
