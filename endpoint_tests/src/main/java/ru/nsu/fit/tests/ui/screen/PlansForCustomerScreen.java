package ru.nsu.fit.tests.ui.screen;

import ru.nsu.fit.services.browser.Browser;

import java.net.URI;

public class PlansForCustomerScreen {
    private static final String url = "/endpoint/plans_for_customer.html";
    private Browser browser;

    public PlansForCustomerScreen(Browser browser) {
        this.browser = browser;

        this.checkUrl();
    }

    private void checkUrl() {
        URI u = browser.getCurrentPageUrl();
        String path = u.getPath();
        if (!url.equals(path)) {
            throw new RuntimeException("wrong url: " + path);
        }
    }

    public String getDataFromTable(int column) {
        return browser.getDataFromTable(column);
    }

    public boolean isTableEmpty() {
        return browser.isTableEmpty();
    }

    public void subscribe() {
        browser.clickButtonInTable();
    }
}
