package ru.nsu.fit.tests.ui;

import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.api.BuildVerificationTest;
import ru.nsu.fit.tests.ui.screen.AddCustomerScreen;
import ru.nsu.fit.tests.ui.screen.AddPlanScreen;
import ru.nsu.fit.tests.ui.screen.CustomersScreen;
import ru.nsu.fit.tests.ui.screen.LoginScreen;
import ru.nsu.fit.tests.ui.screen.PlansForCustomerScreen;
import ru.nsu.fit.tests.ui.screen.PlansScreen;
import ru.nsu.fit.tests.ui.screen.SubscriptionsScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.core.Response;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class SubUnsubTest {
    private final static String EMAIL = "email@example.com";
    private final static String PASSWORD = "strongpass";
    private final static String PLAN_NAME = "Simple plan";
    private final static String PLAN_DETAILS = "none";
    private final static int PLAN_COST = 50;
    private Browser browser = null;
    private DBService dbService;

    @BeforeClass
    public void beforeClass() {
        dbService = new DBService(LoggerFactory.getLogger(DBService.class));
        dbService.cleanDatabase();

        CustomerPojo customer = new CustomerPojo();
        customer.firstName = "Johnds";
        customer.lastName = "Weak";
        customer.login = EMAIL;
        customer.pass = PASSWORD;
        customer.balance = 0;

        Response response = BuildVerificationTest.postByAdmin("create_customer", customer);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
        customer = response.readEntity(CustomerPojo.class);
        response = BuildVerificationTest.postByAdmin("increase_customer_balance_by_login/" + customer.login + "/100", null);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        PlanPojo plan = new PlanPojo();
        plan.name = PLAN_NAME;
        plan.details = PLAN_DETAILS;
        plan.fee = PLAN_COST;

        response = BuildVerificationTest.postByAdmin("create_plan", plan);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        browser = BrowserService.openNewBrowser();
    }

    @Test
    @Title("Check no subs")
    @Description("Check that before subbing there were no subs")
    @Severity(SeverityLevel.BLOCKER)
    public void checkNoSubs() {
        this.browser.openPage("http://localhost:8080/endpoint/");
        LoginScreen loginScreen = new LoginScreen(browser);
        loginScreen.enterEmail(EMAIL);
        loginScreen.enterPassword(PASSWORD);
        loginScreen.clickLogin();

        this.browser.openPage("http://127.0.0.1:8080/endpoint/subscriptions.html?login=email%40example.com");

        SubscriptionsScreen screen = new SubscriptionsScreen(browser);
        Assert.assertTrue(screen.isTableEmpty());
    }

    @Test(dependsOnMethods = "checkNoSubs")
    @Title("Check subbing")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void checkSubAppears() {
        this.browser.openPage("http://localhost:8080/endpoint/plans_for_customer.html?login=email%40example.com");
        PlansForCustomerScreen plans = new PlansForCustomerScreen(browser);
        plans.subscribe();

        this.browser.openPage("http://127.0.0.1:8080/endpoint/subscriptions.html?login=email%40example.com");
        SubscriptionsScreen screen = new SubscriptionsScreen(browser);
        Assert.assertFalse(screen.isTableEmpty());
    }

    @Test(dependsOnMethods = "checkSubAppears")
    @Title("Check unsubbing")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void checkSubDisappear() {
        SubscriptionsScreen screen = new SubscriptionsScreen(browser);
        screen.unsubscribe();
        Assert.assertEquals(screen.getDataFromTable(2), "Unsubbed");

        this.browser.openPage("http://127.0.0.1:8080/endpoint/subscriptions.html?login=email%40example.com");
        screen = new SubscriptionsScreen(browser);
        Assert.assertTrue(screen.isTableEmpty());
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (dbService != null) {
            dbService.cleanDatabase();
        }

        if (browser != null) {
            browser.close();
        }
    }
}
