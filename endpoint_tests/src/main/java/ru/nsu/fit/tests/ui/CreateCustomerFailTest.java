package ru.nsu.fit.tests.ui;

import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.ui.screen.AddCustomerScreen;
import ru.nsu.fit.tests.ui.screen.CustomersScreen;
import ru.nsu.fit.tests.ui.screen.LoginScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CreateCustomerFailTest {
    private Browser browser = null;
    private DBService dbService;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();

        dbService = new DBService(LoggerFactory.getLogger(DBService.class));
        dbService.cleanDatabase();

        this.browser.openPage("http://localhost:8080/endpoint/");

        CustomersScreen customersScreen = LoginScreen.enterAsAdmin(browser);
        customersScreen.clickAddNewCustomer();
    }

    private void fillTestInfo(AddCustomerScreen addCustomerScreen) {
        addCustomerScreen.enterFirstName("John");
        addCustomerScreen.enterLastName("Weak");
        addCustomerScreen.enterEmail("email@example.com");
        addCustomerScreen.enterPassword("strongpass");
    }

    @Test
    @Title("Create Customer fail by name")
    @Severity(SeverityLevel.MINOR)
    public void failByFirstName() {
        AddCustomerScreen addCustomerScreen = new AddCustomerScreen(browser);
        this.fillTestInfo(addCustomerScreen);
        addCustomerScreen.enterFirstName("john");
        addCustomerScreen.clickCreateCustomer();

        String alert = browser.getAlertMessage();
        Assert.assertEquals(alert, "FirstName's first letter must be capital, other letters must be in lower case, there must be from 2 to 12 letters inclusively");
    }

    @Test(priority = 1)
    @Title("Create Customer fail by email")
    @Severity(SeverityLevel.MINOR)
    @Features("Customer feature")
    public void failByUsedEmail() {
        AddCustomerScreen addCustomerScreen = new AddCustomerScreen(browser);
        this.fillTestInfo(addCustomerScreen);
        addCustomerScreen.clickCreateCustomer();
        new CustomersScreen(browser).clickAddNewCustomer();

        addCustomerScreen = new AddCustomerScreen(browser);
        this.fillTestInfo(addCustomerScreen);
        addCustomerScreen.clickCreateCustomer();

        String alert = browser.getAlertMessage();
        Assert.assertEquals(alert, "Login should be unique");
    }

    @Test
    @Title("Create Customer fail by weak password")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void failByWeakPassword() {
        AddCustomerScreen addCustomerScreen = new AddCustomerScreen(browser);
        this.fillTestInfo(addCustomerScreen);
        addCustomerScreen.enterPassword("123qwe");
        addCustomerScreen.clickCreateCustomer();

        String alert = browser.getAlertMessage();
        Assert.assertEquals(alert, "Password is easy");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (dbService != null) {
            dbService.cleanDatabase();
        }

        if (browser != null) {
            browser.close();
        }
    }
}
