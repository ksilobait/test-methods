package ru.nsu.fit.tests.api;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

public class PlanManagerApiTest extends BuildVerificationTest {
    private UUID planId;

    @BeforeClass
    public void prepareCustomer() {
        CustomerPojo customer = new CustomerPojo();
        customer.firstName = "Johnds";
        customer.lastName = "Weak";
        customer.login = "helloworld123@login.com";
        customer.pass = "password123";
        customer.balance = 0;

        Response response = postByAdmin("create_customer", customer);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        response = postByAdmin("increase_customer_balance_by_login/" + customer.login + "/10000", customer);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }

    @Test(description = "Create the plan via API.")
    public void createPlan() {
        PlanPojo plan = new PlanPojo();
        plan.name = "Best plan ever";
        plan.details = "This plan let's you do whatever you want, and it's cheap";
        plan.fee = 1;

        Response response = postByAdmin("create_plan", plan);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }

    @Test(description = "Get the plan via API.", dependsOnMethods = "createPlan")
    public void getPlan() {
        Response response = getByAdmin("get_plans_for_customer_by_login/helloworld123@login.com");

        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
        PlanPojo plan = response.readEntity(new GenericType<List<PlanPojo>>() {
        }).get(0);
        planId = plan.id;

        PlanPojo expected = new PlanPojo();
        expected.name = "Best plan ever";
        expected.details = "This plan let's you do whatever you want, and it's cheap";
        expected.fee = 1;

        Assert.assertEquals(plan.name, expected.name);
        Assert.assertEquals(plan.details, expected.details);
        Assert.assertEquals(plan.fee, expected.fee);
    }

    @Test(description = "Update the plan via API.", dependsOnMethods = "getPlan")
    public void updatePlan() {
        Response response = getByAdmin("get_plans_for_customer_by_login/helloworld123@login.com");
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        PlanPojo plan = new PlanPojo();
        plan.id = planId;
        plan.name = "Wow even better";
        plan.details = "Expensive, but elite";
        plan.fee = 1337;

        Response secondResponse = postByAdmin("update_plan", plan);
        Assert.assertEquals(secondResponse.getStatusInfo(), Response.Status.OK);
        PlanPojo updated = secondResponse.readEntity(PlanPojo.class);

        Assert.assertEquals(updated.name, plan.name);
        Assert.assertEquals(updated.details, plan.details);
        Assert.assertEquals(updated.fee, plan.fee);
    }

    @Test(description = "Remove the plan via API.", dependsOnMethods = {"updatePlan", "getPlan", "createPlan"})
    public void removeCustomer() {
        Response response = postByAdmin("/remove_plan_by_id/" + planId, null);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }
}
