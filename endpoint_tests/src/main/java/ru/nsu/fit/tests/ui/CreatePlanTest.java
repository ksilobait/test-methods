package ru.nsu.fit.tests.ui;

import static ru.nsu.fit.endpoint.shared.Globals.ADMIN_LOGIN;
import static ru.nsu.fit.endpoint.shared.Globals.ADMIN_PASS;

import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.ui.screen.AddPlanScreen;
import ru.nsu.fit.tests.ui.screen.CustomersScreen;
import ru.nsu.fit.tests.ui.screen.LoginScreen;
import ru.nsu.fit.tests.ui.screen.PlansScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CreatePlanTest {
    private Browser browser = null;
    private DBService dbService;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();

        dbService = new DBService(LoggerFactory.getLogger(DBService.class));
        dbService.cleanDatabase();
    }

    @Test
    @Title("Create Plan")
    @Description("Create plan via UI API")
    @Severity(SeverityLevel.BLOCKER)
    public void createPlan() {
        this.browser.openPage("http://localhost:8080/endpoint/");

        LoginScreen loginScreen = new LoginScreen(browser);
        loginScreen.enterEmail(ADMIN_LOGIN);
        loginScreen.enterPassword(ADMIN_PASS);
        loginScreen.clickLogin();

        CustomersScreen customersScreen = new CustomersScreen(browser);
        customersScreen.clickShowPlans();

        PlansScreen plansScreen = new PlansScreen(browser);
        plansScreen.clickAddNewPlan();

        AddPlanScreen addPlanScreen = new AddPlanScreen(browser);
        addPlanScreen.enterName("Wow even better");
        addPlanScreen.enterDetails("Expensive, but elite");
        addPlanScreen.enterFee("1337");
        addPlanScreen.clickCreatePlan();
    }

    @Test(dependsOnMethods = "createPlan")
    @Title("Check plan creation")
    @Description("Get plan id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Plan feature")
    public void checkPlan() {
        PlansScreen plansScreen = new PlansScreen(browser);
        Assert.assertEquals(plansScreen.getDataFromTable(1), "Wow even better");
        Assert.assertEquals(plansScreen.getDataFromTable(2), "Expensive, but elite");
        Assert.assertEquals(plansScreen.getDataFromTable(3), "1337");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (dbService != null) {
            dbService.cleanDatabase();
        }

        if (browser != null) {
            browser.close();
        }
    }
}
