package ru.nsu.fit.tests.api;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public abstract class BuildVerificationTest {
    protected DBService dbService;

    public static Invocation.Builder getAdminInvocationBuilder(String path/*, Map<String, Object> params*/) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(HttpAuthenticationFeature.basic("admin", "setup"));
        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient(clientConfig);
        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path(path);
        return webTarget.request(MediaType.APPLICATION_JSON);
    }

    public static Response postByAdmin(String path, Object body) {
        Invocation.Builder invocationBuilder = getAdminInvocationBuilder(path);
        Response response = invocationBuilder.post(Entity.entity(body, MediaType.APPLICATION_JSON));
        response.bufferEntity();
        Logger.info("POST: response= " + response.readEntity(String.class));
        return response;
    }

    public static Response getByAdmin(String path) {
        Invocation.Builder invocationBuilder = getAdminInvocationBuilder(path);
        Response response = invocationBuilder.get();
        response.bufferEntity();
        Logger.info("GET: response= " + response.readEntity(String.class));
        return response;
    }

    @BeforeClass
    protected void beforeClass() {
        dbService = new DBService(LoggerFactory.getLogger(DBService.class));
        dbService.cleanDatabase();
    }

    @AfterClass
    protected void afterClass() {
        dbService.cleanDatabase();
    }
}
