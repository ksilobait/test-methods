package ru.nsu.fit.tests.ui.screen;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

import java.net.URI;

public class SubscriptionsScreen {
    private static final String url = "/endpoint/subscriptions.html";
    private By addNewPlanButton;
    private Browser browser;

    public SubscriptionsScreen(Browser browser) {
        this.browser = browser;

        this.checkUrl();
    }

    private void checkUrl() {
        URI u = browser.getCurrentPageUrl();
        String path = u.getPath();
        if (!url.equals(path)) {
            throw new RuntimeException("wrong url: " + path);
        }
    }

    public String getDataFromTable(int column) {
        return browser.getDataFromTable(column);
    }

    public boolean isTableEmpty() {
        return browser.isTableEmpty();
    }

    public void unsubscribe() {
        browser.clickButtonInTable();
    }
}
