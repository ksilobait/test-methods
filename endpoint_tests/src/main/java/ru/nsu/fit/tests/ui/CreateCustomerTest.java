package ru.nsu.fit.tests.ui;

import static ru.nsu.fit.endpoint.shared.Globals.ADMIN_LOGIN;
import static ru.nsu.fit.endpoint.shared.Globals.ADMIN_PASS;

import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.ui.screen.AddCustomerScreen;
import ru.nsu.fit.tests.ui.screen.CustomersScreen;
import ru.nsu.fit.tests.ui.screen.LoginScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CreateCustomerTest {
    private Browser browser = null;
    private DBService dbService;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();

        dbService = new DBService(LoggerFactory.getLogger(DBService.class));
        dbService.cleanDatabase();
    }

    @Test
    @Title("Create Customer")
    @Description("Create customer via UI API")
    @Severity(SeverityLevel.BLOCKER)
    public void createCustomer() {
        this.browser.openPage("http://localhost:8080/endpoint/");

        LoginScreen loginScreen = new LoginScreen(browser);
        loginScreen.enterEmail(ADMIN_LOGIN);
        loginScreen.enterPassword(ADMIN_PASS);
        loginScreen.clickLogin();

        CustomersScreen customersScreen = new CustomersScreen(browser);
        customersScreen.clickAddNewCustomer();

        AddCustomerScreen addCustomerScreen = new AddCustomerScreen(browser);
        addCustomerScreen.enterFirstName("John");
        addCustomerScreen.enterLastName("Weak");
        addCustomerScreen.enterEmail("email@example.com");
        addCustomerScreen.enterPassword("strongpass");
        addCustomerScreen.clickCreateCustomer();
    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Check customer creation")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkCustomer() {
        CustomersScreen customersScreen = new CustomersScreen(browser);
        Assert.assertEquals(customersScreen.getDataFromTable(1), "John");
        Assert.assertEquals(customersScreen.getDataFromTable(2), "Weak");
        Assert.assertEquals(customersScreen.getDataFromTable(3), "email@example.com");
        Assert.assertEquals(customersScreen.getDataFromTable(4), "strongpass");
    }

    @Test(dependsOnMethods = "checkCustomer")
    @Title("Check balance increase")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkBalanceIncrease() {
        CustomersScreen customersScreen = new CustomersScreen(browser);
        customersScreen.increaseBalance();
        Assert.assertEquals(customersScreen.getDataFromTable(5), "100");
        customersScreen.increaseBalance();
        Assert.assertEquals(customersScreen.getDataFromTable(5), "200");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (dbService != null) {
            dbService.cleanDatabase();
        }

        if (browser != null) {
            browser.close();
        }
    }
}
