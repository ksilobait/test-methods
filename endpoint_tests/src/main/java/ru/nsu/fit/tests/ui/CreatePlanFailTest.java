package ru.nsu.fit.tests.ui;

import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.ui.screen.AddPlanScreen;
import ru.nsu.fit.tests.ui.screen.CustomersScreen;
import ru.nsu.fit.tests.ui.screen.LoginScreen;
import ru.nsu.fit.tests.ui.screen.PlansScreen;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CreatePlanFailTest {
    private Browser browser = null;
    private DBService dbService;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();

        dbService = new DBService(LoggerFactory.getLogger(DBService.class));
        dbService.cleanDatabase();

        this.browser.openPage("http://localhost:8080/endpoint/");

        CustomersScreen customersScreen = LoginScreen.enterAsAdmin(browser);
        customersScreen.clickShowPlans();

        PlansScreen plansScreen = new PlansScreen(browser);
        plansScreen.clickAddNewPlan();

    }

    private void fillTestInfo(AddPlanScreen screen) {
        screen.enterName("My plan");
        screen.enterDetails("It's brilliant");
        screen.enterFee("10");
    }

    @Test
    @Title("Create Plan fail by name")
    @Severity(SeverityLevel.MINOR)
    public void failByName() {
        AddPlanScreen screen = new AddPlanScreen(browser);
        this.fillTestInfo(screen);
        screen.enterName("qwe $");
        screen.clickCreatePlan();

        String alert = browser.getAlertMessage();
        Assert.assertEquals(alert, "name has special symbols");
    }

    @Test
    @Title("Create Plan fail by fee not a number")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void failByFeeNotParsable() {
        AddPlanScreen screen = new AddPlanScreen(browser);
        this.fillTestInfo(screen);
        screen.enterFee("q123");
        screen.clickCreatePlan();

        String alert = browser.getAlertMessage();
        Assert.assertEquals(alert, "fee is not a number");
    }

    @Test
    @Title("Create Plan fail by fee < 0")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void failByFeeNegative() {
        AddPlanScreen screen = new AddPlanScreen(browser);
        this.fillTestInfo(screen);
        screen.enterFee("-10");

        screen.clickCreatePlan();

        String alert = browser.getAlertMessage();
        Assert.assertEquals(alert, "the fee should be between 0 and 999999");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (dbService != null) {
            dbService.cleanDatabase();
        }

        if (browser != null) {
            browser.close();
        }
    }
}
