package ru.nsu.fit.tests.ui.screen;

import java.net.URI;
import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;

public class AddPlanScreen {
    private static final String url = "/endpoint/add_plan.html";
    private final Browser browser;
    private By nameField;
    private By detailsField;
    private By feeField;
    private By createPlanButton;

    public AddPlanScreen(Browser browser) {
        this.browser = browser;
        this.checkUrl();

        nameField = By.id("name_id");
        detailsField = By.id("details_id");
        feeField = By.id("fee_id");
        createPlanButton = By.id("create_plan_id");
    }

    private void checkUrl() {
        URI u = browser.getCurrentPageUrl();
        String path = u.getPath();
        if (!url.equals(path)) {
            throw new RuntimeException("wrong url: " + path);
        }
    }

    public void enterName(String name) {
        browser.typeText(nameField, name);
    }

    public void enterDetails(String details) {
        browser.typeText(detailsField, details);
    }

    public void enterFee(String fee) {
        browser.typeText(feeField, fee);
    }

    public void clickCreatePlan() {
        browser.click(createPlanButton);
    }

}
