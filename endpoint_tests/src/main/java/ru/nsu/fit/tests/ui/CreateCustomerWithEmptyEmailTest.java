package ru.nsu.fit.tests.ui;

import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.DBService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.ui.screen.AddCustomerScreen;
import ru.nsu.fit.tests.ui.screen.CustomersScreen;
import ru.nsu.fit.tests.ui.screen.LoginScreen;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CreateCustomerWithEmptyEmailTest {
    private Browser browser = null;
    private DBService dbService;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();

        dbService = new DBService(LoggerFactory.getLogger(DBService.class));
        dbService.cleanDatabase();
    }

    @Test
    @Title("Create Customer With Empty Email")
    @Severity(SeverityLevel.BLOCKER)
    public void createCustomerWithEmptyEmail() {
        this.browser.openPage("http://localhost:8080/endpoint/");

        CustomersScreen customersScreen = LoginScreen.enterAsAdmin(browser);
        customersScreen.clickAddNewCustomer();

        AddCustomerScreen addCustomerScreen = new AddCustomerScreen(browser);
        addCustomerScreen.enterFirstName("John");
        addCustomerScreen.enterLastName("Weak");
        addCustomerScreen.enterEmail("");
        addCustomerScreen.enterPassword("strongpass");
        addCustomerScreen.clickCreateCustomer();
        Assert.assertEquals(browser.getAlertMessage(), "Email or password is empty");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (dbService != null) {
            dbService.cleanDatabase();
        }

        if (browser != null) {
            browser.close();
        }
    }
}
