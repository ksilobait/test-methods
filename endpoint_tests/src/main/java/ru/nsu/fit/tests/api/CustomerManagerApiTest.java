package ru.nsu.fit.tests.api;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.core.Response;

public class CustomerManagerApiTest extends BuildVerificationTest {

    @Test(description = "Create the customer via API.")
    public void createCustomer() {
        CustomerPojo customer = new CustomerPojo();
        customer.firstName = "Johnds";
        customer.lastName = "Weak";
        customer.login = "helloworld123@login.com";
        customer.pass = "password123";
        customer.balance = 0;

        Response response = postByAdmin("create_customer", customer);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }

    @Test(description = "Get the customer via API.", dependsOnMethods = "createCustomer")
    public void getCustomer() {
        // TODO: попробовать получить данные для созданного на предыдущем шаге customer.
        Response response = getByAdmin("get_customer_by_login/helloworld123@login.com");

        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
        CustomerPojo customer = response.readEntity(CustomerPojo.class);

        CustomerPojo expected = new CustomerPojo();
        expected.firstName = "Johnds";
        expected.lastName = "Weak";
        expected.login = "helloworld123@login.com";
        expected.pass = "password123";
        expected.balance = 0;

        Assert.assertEquals(customer.firstName, expected.firstName);
        Assert.assertEquals(customer.lastName, expected.lastName);
        Assert.assertEquals(customer.login, expected.login);
        Assert.assertEquals(customer.pass, expected.pass);
        Assert.assertEquals(customer.balance, expected.balance);
    }

    @Test(description = "Update the customer via API.", dependsOnMethods = "getCustomer")
    public void updateCustomer() {
        Response response = getByAdmin("get_customer_by_login/helloworld123@login.com");
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);

        CustomerPojo customer = response.readEntity(CustomerPojo.class);
        customer.firstName = "John";
        customer.lastName = "Rich";

        Response secondResponse = postByAdmin("update_customer", customer);
        Assert.assertEquals(secondResponse.getStatusInfo(), Response.Status.OK);
        CustomerPojo updatedCustomer = secondResponse.readEntity(CustomerPojo.class);

        CustomerPojo expected = new CustomerPojo();
        expected.firstName = "John";
        expected.lastName = "Rich";
        expected.login = "helloworld123@login.com";
        expected.pass = "password123";

        Assert.assertEquals(updatedCustomer.firstName, expected.firstName);
        Assert.assertEquals(updatedCustomer.lastName, expected.lastName);
        Assert.assertEquals(updatedCustomer.login, expected.login);
        Assert.assertEquals(updatedCustomer.pass, expected.pass);
    }

    @Test(description = "Remove the customer via API.", dependsOnMethods = {"updateCustomer", "getCustomer", "createCustomer"})
    public void removeCustomer() {
        Response response = postByAdmin("/remove_customer_by_login/helloworld123@login.com", null);
        Assert.assertEquals(response.getStatusInfo(), Response.Status.OK);
    }
}
