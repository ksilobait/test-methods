package ru.nsu.fit.services.browser;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.nsu.fit.shared.AllureUtils;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Please read: https://github.com/SeleniumHQ/selenium/wiki/Grid2
 *
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Browser implements Closeable {
    private WebDriver webDriver;
    private String lastAlertText = null;

    public Browser() {
        // create web driver
        try {
            ChromeOptions chromeOptions = new ChromeOptions();

            // for running in Docker container as 'root'.
            chromeOptions.addArguments("no-sandbox");
            chromeOptions.addArguments("disable-dev-shm-usage");
            chromeOptions.addArguments("disable-setuid-sandbox");
            chromeOptions.addArguments("disable-infobars");

            // https://stackoverflow.com/questions/48450594/selenium-timed-out-receiving-message-from-renderer/52340526#52340526
            ChromeOptions options = chromeOptions;
            // ChromeDriver is just AWFUL because every version or two it breaks unless you pass cryptic arguments
            //AGRESSIVE: options.setPageLoadStrategy(PageLoadStrategy.NONE); // https://www.skptricks.com/2018/08/timed-out-receiving-message-from-renderer-selenium.html
//            options.addArguments("start-maximized"); // https://stackoverflow.com/a/26283818/1689770
            options.addArguments("enable-automation"); // https://stackoverflow.com/a/43840128/1689770
            options.addArguments("--disable-extensions");
            options.addArguments("--dns-prefetch-disable");
//            options.addArguments("--headless"); // only if you are ACTUALLY running headless
            options.addArguments("--no-sandbox"); //https://stackoverflow.com/a/50725918/1689770
            options.addArguments("--disable-infobars"); //https://stackoverflow.com/a/43840128/1689770
            options.addArguments("--disable-dev-shm-usage"); //https://stackoverflow.com/a/50725918/1689770
            options.addArguments("--disable-browser-side-navigation"); //https://stackoverflow.com/a/49123152/1689770
            options.addArguments("--disable-gpu"); //https://stackoverflow.com/questions/51959986/how-to-solve-selenium-chromedriver-timed-out-receiving-message-from-renderer-exc
//            options.setPageLoadStrategy(PageLoadStrategy.EAGER);

            // enable no more than one of the following two:
//            options.addArguments("enable-features=NetworkServiceInProcess");
//            options.addArguments("disable-features=NetworkService");


            chromeOptions.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

            chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            chromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

            // we use Windows platform for development only and not for AT launch.
            // For launch AT regression, we use Linux platform.
            if (System.getProperty("os.name").toLowerCase().contains("win")) {
                System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
                chromeOptions.setHeadless(Boolean.parseBoolean(System.getProperty("headless")));
                webDriver = new ChromeDriver(chromeOptions);
            } else {
                File f = new File("/usr/bin/chromedriver");
                if (f.exists()) {
                    chromeOptions.addArguments("single-process");
                    chromeOptions.addArguments("headless");
                    System.setProperty("webdriver.chrome.driver", f.getPath());
                    webDriver = new ChromeDriver(chromeOptions);
                }
            }

            if (webDriver == null) {
                throw new RuntimeException();
            }

            webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        } catch (Exception ex) {
            close();
            throw new RuntimeException(ex);
        }
    }

    public URI getCurrentPageUrl() {
        return URI.create(webDriver.getCurrentUrl());
    }

    public Browser openPage(String url) {
        webDriver.get(url);
        return this;
    }

    public Browser waitForElement(By element) {
        return waitForElement(element, 10);
    }

    public Browser waitForElement(final By element, int timeoutSec) {
        WebDriverWait wait = new WebDriverWait(webDriver, timeoutSec);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        return this;
    }

    public Browser click(By element) {
        makeScreenshot(before("clicking on ", element));
        webDriver.findElement(element).click();
        makeScreenshot(before("after on ", element));
        return this;
    }

    public Browser typeText(By element, String text) {
        makeScreenshot(before("typing to ", element));
        WebElement webElement = webDriver.findElement(element);
        webElement.clear();
        webElement.sendKeys(text);
        makeScreenshot(after("typing to ", element));
        return this;
    }

    public WebElement getElement(By element) {
        return webDriver.findElement(element);
    }

    public String getValue(By element) {
        return getElement(element).getAttribute("value");
    }

    public List<WebElement> getElements(By element) {
        return webDriver.findElements(element);
    }

    public boolean isElementPresent(By element) {
        return getElements(element).size() != 0;
    }

    private void waitTheFuckingBuggyChrome() {
        // Ебучий хром не работает.
        // Ебучий драйвер для хрома падает.
        // Ебучий Selenium ничего не может с этим сделать.
        // Ебучий гугл пишет говнокод с багами, решения которых нет в его ебучем поиске.
        // Эта функция нужна, без нее нихуя не работает.
        // Количество времени, убитое на то, чтобы победить ебучие баги самого ебучего браузера самой ебучей корпорации:
        // 4 часа
        // Да, это сраное говнище тупо падает, если ему слишком часто слать запросы.
        try {
            Thread.sleep(16);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void makeScreenshot(String fileName) {
        waitTheFuckingBuggyChrome();
        takeOffTheAlert();
        TakesScreenshot ts = ((TakesScreenshot) webDriver);
        byte[] image = ts.getScreenshotAs(OutputType.BYTES);
        AllureUtils.saveImageAttach(fileName + ".png", image);
    }

    private void takeOffTheAlert() {
        try {
            Alert alert = webDriver.switchTo().alert();
            String message = alert.getText();
            alert.dismiss();
            lastAlertText = message;
        } catch (NoAlertPresentException Ex) {
        }
    }

    private String clearAlert() {
        String alertText = lastAlertText;
        lastAlertText = null;
        return alertText;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public String getDataFromTable(int column) {
        return webDriver.findElement(By.xpath(String.format("(//table//td)[%d]\n", column))).getText();
    }

    public void clickButtonInTable() {
        String path = "(//table//td//button)\n";
        webDriver.findElement(By.xpath(path)).click();
    }

    public boolean isTableEmpty() {
        String path = "(//table//td)[2]\n";
        List<WebElement> elements = webDriver.findElements(By.xpath(path));
        return elements.isEmpty();
    }

    private boolean waitForAlert() {
        for (int i = 0; i < 5; i++) {
            try {
                Alert alert = webDriver.switchTo().alert();
                return true;
            } catch (NoAlertPresentException e) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
        return false;
    }

    public String getAlertMessage() {
        if (lastAlertText != null) {
            return clearAlert();
        }

        if (this.waitForAlert()) {
            takeOffTheAlert();
            return clearAlert();
        }

        return "";
    }

    private String before(String action, By element) {
        return "before " + action + " " + element.toString();
    }

    private String after(String action, By element) {
        return "after " + action + " " + element.toString();
    }

    @Override
    public void close() {
        if (webDriver != null) {
            webDriver.close();
            webDriver.quit();
        }

        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            try {
                // kill it with fire!
                Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
